source ${SIN_SCRIPTS_DIR}/python.sh

function __sin-generic-run-all() {
    local prefix=${1}

    if [[ -z ${prefix} ]]; then
        echo >&2 "Pass function prefix"
        return 1
    fi

    for name in $(sin-get-func-options ${prefix}-); do
        echo "Running ${prefix}-${name}"
        ${prefix}-${name}
    done
}

function __sin-run-all-from-funcname() {
    __sin-generic-run-all $(sin-get-funcname 2)
}

function sin-cd() {
    cd ${1}

    sin-pypoetry-autosetup 2>& /dev/null
    if [[ -f .env ]]; then
        source .env
    fi
}

function sin-size() {
    local dir=${1:-'.'}

    if [[ ! -d ${dir} ]]; then
        echo >&2 "${dir} doesn't exist"
        return 1
    fi

    du ${dir} -had 1 | sort -hr
}

function sin-rmdir() {
    local dir=${1:-'.'}

    if [[ -d ${dir} ]]; then
        local dirSize=$(sin-size ${dir} |
            tail -n 1 |
            sed -nr 's/([[:digit:]]+[[:alpha:]]+)[[:blank:]]+.*/\1/p')

        echo "${dirSize} of data will be removed from $(realpath ${dir})"
        rm -r ${dir}
    fi
}

function sin-get-func-options() {
    local prefix=${1}

    if [[ -z ${prefix} ]]; then
        echo >&2 "Pass function prefix"
        return 1
    fi

    declare -f | sed -rn "s/^${prefix}([a-zA-Z]+).*/\1/p" | uniq
}

function sin-check-exec() {
    local name=${1}

    if [[ -z ${name} ]]; then
        echo >&2 "Pass name of the program you want to check"
        return 1
    fi

    if [[ ! $(command -v ${name}) ]]; then
        echo >&2 '"'${name}'"' "is not installed"
        return 2
    fi

}

function sin-shellname() {
    ps -p $$ | tail -n 1 | awk '{ print $4 }' | tr -d '-'
}

function sin-get-funcname() {
    local stackIdx=${1:-1}
    local shellname=$(sin-shellname)

    case $shellname in
    "zsh") funcname=$funcstack[$((${stackIdx} + 1))] ;;
    "bash") funcname=${FUNCNAME[${stackIdx}]} ;;
    *) echo >&2 "Shell not defined" && return 1 ;;
    esac

    if [[ -z ${funcname} ]]; then
        echo >&2 "Function probably has been run in shell not in a function"
        return 1
    fi

    echo ${funcname}
}

function sin-rm-cache() {
    __sin-run-all-from-funcname
}

function sin-socials() {
    socialDesktop=2
    apps=(telegram-desktop slack mattermost-desktop discord)
    
    for app in ${apps}; do
        if [[ -z $(sin-check-exec ${app}) ]]; then
            kstart --desktop ${socialDesktop} ${app}
        fi
    done
}
