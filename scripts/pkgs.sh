function sin-rm-cache-linux-pacman() {
    ! sin-check-exec pacman && return 1

    sudo pacman -Sc --noconfirm 1>/dev/null
}

function sin-rm-cache-linux-yay() {
    ! sin-check-exec yay && return 1

    yay -Sc --noconfirm 1>/dev/null
}

function sin-rm-cache-linux() {
    __sin-generic-run-all sin-rm-cache-linux
}

function sin-pacman-list-unused-deps() {
    ! sin-check-exec pacman && return 1

    pacman -Qdtq
}

function sin-check-and-install-exec() {
    local pkg=${1}

    if [[ -z ${pkg} ]]; then
        echo >&2 "Pass package name"
        return 1
    fi

    if ! sin-check-exec ${pkg}; then
        sin-install ${pkg}
    fi

    return $(sin-check-exec ${pkg})
}

function sin-linux-autoremove() {
    ! sin-check-exec pacman && return 1

    local pkgsToRemove=($(sin-pacman-list-unused-deps))

    if [[ -z ${pkgsToRemove} ]]; then
        echo >&2 "There are no packages to remove"
        return 1
    fi

    sudo pacman -Rsc ${pkgsToRemove}
}
