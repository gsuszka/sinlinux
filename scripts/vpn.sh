function sin-vpn() {
    local certPath=${1}

    if [[ ! -f ${certPath} ]]; then
        echo >&2 "Can't find openvpn cert"
        return 1
    fi

    sudo openvpn --config ${certPath}
}

function sin-vpn-work() {
    local workName=${1}

    sin-vpn $(
        find ${SIN_VPN_CERT_ROOT}/${workName} \
            -type f \
            -name "*.ovpn" \
            -print \
            -quit
    )
}
