function sin-scala-rm-bloops() {
    local root=${1:-'.'}
    local dirs=($(find -type d -name '.bloop'))

    for dir in ${dirs}; do
        sin-rmdir ${dir}
    done
}

function sin-rm-cache-scala() {
    sin-scala-rm-bloops
}
