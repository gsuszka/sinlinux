function sin-reboot-to-windows() {
    sudo sed -r 's/(GRUB_DEFAULT=).*/\1saved/g' -i /etc/default/grub

    if grep -q GRUB_DISABLE_OS_PROBER /etc/default/grub 1>/dev/null; then
        sudo sed -r 's/(GRUB_DISABLE_OS_PROBER=)true/\1false/g' -i /etc/default/grub
    else
        echo GRUB_DISABLE_OS_PROBER=false | sudo tee -a /etc/default/grub
    fi

    sudo update-grub

    local winEntry=$(grep menuentry /boot/grub/grub.cfg | grep --line-number Windows)
    local menuNumber=$(echo ${winEntry} | sed -e "s/:.*//")

    sudo grub-reboot ${menuNumber}
    sudo reboot
}
