# PIP

function sin-python-install-pip() {
    curl https://bootstrap.pypa.io/get-pip.py | python3 -
    pip install --no-cache-dir wheel
}

# PYPOETRY
function sin-python-install-poetry() {
    curl -sSL https://install.python-poetry.org | python3 -
}

function sin-pypoetry-find-projects() {
    local rootDir=${1:-"."}

    local dirs=$(find ${rootDir} -name 'pyproject.toml' -exec dirname {} \;)

    if [[ ! ${dirs} ]]; then
        echo >&2 "Couldn't find any projects"
        return 1
    fi

    echo ${dirs}
}

function sin-pypoetry-project-settings() {
    local root=${1:-'.'}

    if [[ ! -f ${root}/pyproject.toml ]]; then
        echo >&2 "Dir is not a poetry project"
        return 1
    fi

    cd ${projectRoot} >/dev/null
    poetry config --local virtualenvs.in-project false
    poetry config --local virtualenvs.create true
    cd - >/dev/null
}

function sin-pypoetry-install() {
    ! sin-check-exec poetry && return 1

    local projectRoot=${1:-"."}
    local projectRoot=$(realpath ${projectRoot})

    if [[ -f ${projectRoot}/pyproject.toml ]]; then
        cd ${projectRoot}

        sin-pypoetry-project-settings
        echo "Installing packages from ${projectRoot}"

        poetry install --all-extras
        echo "All packages from "${projectRoot}" have been installed. Have fun :3"

        cd -
    else
        echo >&2 "Cannot install project from ${projectRoot}"
        return 1
    fi
}

function sin-pypoetry-recursive() {
    ! sin-check-exec poetry && return 1

    local cmd=${1}
    local root=${2:-'.'}
    local poetryProjects=($(sin-pypoetry-find-projects ${root}))

    if [[ -z ${poetryProjects} ]]; then
        echo >&2 "Couldn't find any poetry projects in ${root}"
        return 1
    fi

    for project in ${poetryProjects}; do
        
        cd ${project} > /dev/null
        sin-pypoetry-project-settings
        
        echo >&2 "Running \"poetry ${cmd}\" in \"${project}\""
        if [[ ${cmd} == "install" ]]; then
            poetry install --all-extras
        else
            poetry ${cmd}
        fi
        cd - >/dev/null
    done
}

function sin-rm-cache-python-poetry() {
    local cacheDir="${HOME}/.cache/pypoetry/cache"

    if [[ -d ${cacheDir} ]]; then
        sin-rmdir ${cacheDir}
    fi
}

function sin-pypoetry-rm-venvs() {
    local poetryVenvs="${HOME}/.cache/pypoetry/virtualenvs"

    if [[ -z $(ls ${poetryVenvs} 2>/dev/null) ]]; then
        echo "There's no venvs" >&2
        return 1
    fi

    sin-rmdir ${poetryVenvs}
}

function sin-pypoetry-autosetup() {
    ! sin-check-exec poetry && return 1

    if [[ ! -f pyproject.toml ]]; then
        echo >&2 "There's no pyproject.toml"
        return 1
    fi

    [[ -z $(poetry env info -p) ]] && sin-pypoetry-install

    if [[ -z ${POETRY_ACTIVE} ]]; then
        poetry shell
    fi
}

function sin-pypoetry-install-all-extras() {
    ! sin-check-exec poetry && return 1

    if [[ ! -f pyproject.toml ]]; then
        echo >&2 "There's no pyproject.toml"
        return 1
    fi

    poetry install -E "$(sed -n '/^\[tool\.poetry\.extras\]$/,/\[tool/{//!p}' pyproject.toml | sed -En 's/^(.*)\s=.*/\1/p' | tr '\n' ' ' | sed '$s/ $/\n/')"
}

# Python PIP
function sin-rm-cache-python-pip() {
    local cacheDir=${HOME}/.cache/pip

    if [[ -d ${cacheDir} ]]; then
        sin-rmdir ${cacheDir}
    fi
}

# CACHE
function sin-rm-cache-python() {
    __sin-run-all-from-funcname
    sin-rm-pkgs-caches
}

function sin-rm-pkgs-caches() {
    local root=${1:-'.'}
    local cacheDirs=($(find -type d \
        -name '.pytest_cache' -o \
        -name '.mypy_cache' -o \
        -name '__pycache__'))

    for dir in ${cacheDirs}; do
        sin-rmdir ${dir}
    done
}

# VENVS
function sin-rm-venvs-recursive() {
    local venvRootName=${1:-".venv"}
    local rootDir=${2:-"."}

    find ${rootDir} \
        -type d \
        -name ${venvRootName} \
        -exec echo "Removing {}" \; \
        -exec rm -rf {} \;
}
