function __sin-upgrade-spec-yay() {
    yay -Syu --noconfirm --needed --color=auto
}

function __sin-upgrade-spec-snap() {
    sudo snap refresh
}

function __sin-upgrade-spec-omz() {
    omz update &
}

function __sin-upgrade-spec-poetry() {
    poetry self update
}

function __sin-upgrade-print-options() {
    sin-get-func-options __sin-upgrade-spec-
}

function __sin-upgrade-all() {
    to_upgrade=($(__sin-upgrade-print-options))

    for name in ${to_upgrade}; do
        echo "Upgrading ${name}"
        __sin-upgrade-single-item ${name}
    done
}

function __sin-upgrade-single-item() {
    local name=${1}

    if [[ $(command -v ${name}) ]]; then
        __sin-upgrade-spec-${name}
    else
        echo >&2 "Cannot find program named ${name}"
        return 1
    fi
}

function sin-upgrade() {
    local name=${1:-"all"}

    if [[ -z ${name} ]]; then
        echo >&2 "Pass the name of program to upgrade"
        return 1
    fi

    if [[ ${name} == "all" ]]; then
        __sin-upgrade-all
    else
        __sin-upgrade-spec-${name}
    fi
}
