function sin-set-cuda() {
	local cuda_version=${1}

	if [[ -z ${cuda_version} ]]; then
		echo >&2 "You have to pass cuda version"
		return 1
	fi

	if [[ ! -d /opt/cuda-${cuda_version} ]]; then
		possibleCudas=$(find /opt -maxdepth 1 -type d -name 'cuda-*' -print | cut -d/ -f2)
		echo >&2 "There is no cuda-${cuda_version} installed on your computer. Possible are: ${possibleCudas}"
		return 1
	fi

	export PATH=${RVG_PATH}:/opt/cuda-${cuda_version}
}
