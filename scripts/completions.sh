autoload -Uz compinit
compinit

function __sin-set-cuda-completions() {
    COMPREPLY+=(
        $(find -type d \
            -name "/opt/cuda-*" \
            -print)
    )
}

function __sin-vpn-comp-completions() {
    COMPREPLY+=$(find ${SIN_VPN_CERT_ROOT} \
        -type f \
        -name "*.ovpn" \
        -exec realpath --relative-to ${SIN_VPN_CERT_ROOT} {} \; |
        cut -d/ -f1)
}

function __sin-upgrade-completions() {
    COMPREPLY+=("all")
    COMPREPLY+=($(__sin-upgrade-print-options))
}

function __sin-repo-completions() {
    local name

    COMPREPLY=($(sin-find-repos | xargs basename -a 2>&/dev/null | sort | uniq))
}

complete -F __sin-repo-completions sin-repo

complete -F __sin-set-cuda-completions sin-set-cuda
complete -F __sin-vpn-comp-completions sin-vpn-comp
complete -F __sin-upgrade-completions sin-upgrade
complete -d sin-cd
complete -d sin-rmdir
complete -d sin-size
complete -G /dev -d sin-arch-install
