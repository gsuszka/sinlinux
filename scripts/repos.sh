function sin-find-repos() {
    local name=${1}

    if [[ "${name}" == *"/" ]]; then
        name="${name}*"
    else
        name="${name}/"
    fi

    find ${HOME}/repos/ -type d -wholename "*${name}\.git" -exec dirname {} \; | xargs realpath --relative-to=$(pwd) 2>&/dev/null
}

function sin-repo() {
    local name=${1}

    local root=($(sin-find-repos ${name}))

    if [[ ${#root[@]} > 1 ]]; then
        echo "Found more than 1 repo with that name. Specify the name."
        echo -n "- ${root// /\\n- }\n"
        return 1
    fi

    if [[ -z ${root} || ! -d ${root} ]]; then
        echo >&2 "Wrong path ${root}"
        return 1
    fi

    cd ${root}
}

function sin-repo-print-remotes() {
    local repoRoot=${1:-'.'}
    local pretty=${2:-1}

    if [[ ! -d ${repoRoot} ]]; then
        echo >&2 "Directory doesn't exist"
        return 1
    fi

    local repoRoot=$(realpath ${repoRoot})

    if [[ ! -d ${repoRoot}/.git ]]; then
        echo >&2 "Directory is not a git repository"
        return 1
    fi

    if [[ ${pretty} == 1 ]]; then
        echo "Remotes of ${repoRoot##*/}:"
    fi

    cd ${repoRoot}

    for remote in $(git remote show -n); do
        local remote=$(git remote get-url ${remote})

        if [[ ${remote} == "git@"* && -f ${HOME}/.ssh/config ]]; then
            local sshAlias=$(echo ${remote} | sed -rn 's/git@(.*):.*/\1/p')
            local fullHost=$(cat ${HOME}/.ssh/config |
                sed -n "/Host[[:blank:]]*${sshAlias}/,/Host/p" |
                sed -rn 's/Host(Name)?//p' |
                tail -n 1 |
                sed -r 's/[[:blank:]]//g')
            local remote=$(echo ${remote} | sed -rn "s/git@${sshAlias}:(.*)/http:\/\/${fullHost}\/\1/p")
        fi

        if [[ ${pretty} == 1 ]]; then
            echo -n " - "
        fi

        echo ${remote/%.git/}
    done

    cd -
}
