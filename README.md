# Sinight's linux config

## Description

This repo contains useful, at least for me, scripts, shell functions and aliases.

## Installation

To install just `source` the `sinlinux.sh` file in your `$HOME/.$(ps -p $$ | tail -n 1 | awk '{ print $4 }' | tr -d '-')rc`.
