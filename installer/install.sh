#!/bin/env bash
export MAKEFLAGS="-j$(nproc)"

function sin-arch-install() {
    local path=${1}
    local username=${2:-"sinight"}

    if [[ -z ${path} ]]; then
        echo "Pass existing device path"
        return 1
    fi

    echo "Unmounting"
    sudo umount -rl /mnt 2>&1 >/dev/null

    echo "Formatting"
    sudo mkfs.fat -F 32 ${path}1
    sudo mkfs.ext4 ${path}2

    echo "Mounting root"
    sudo mount ${path}2 /mnt
    sudo mkdir -p /mnt/boot/efi
    echo "Mounting boot"
    sudo mount ${path}1 /mnt/boot/efi

    local toPacstrap=(
        base
        base-devel
        linux-headers
        grub
        os-prober
        efibootmgr
        mtools
        dosfstools
        nvidia
        linux-firmware
    )
    echo "Pacstraping ${toPacstrap[@]}"
    sudo pacstrap /mnt ${toPacstrap[@]}

    echo "Generating fstab"
    genfstab -U -p /mnt | sudo tee -a /mnt/etc/fstab

    echo "Copying root chroot script"
    sudo cp -v arch-chroot.sh /mnt/root/
    echo "Chrooting as root"
    sudo arch-chroot /mnt /root/arch-chroot.sh
    echo "Removing root scripts"
    sudo rm -fv /mnt/root/*.sh

    echo "Copying user chroot script"
    sudo cp -v user.sh /mnt/home/${username}/
    echo "Chrooting as ${username}"
    sudo arch-chroot -u ${username}:users /mnt /home/${username}/user.sh
    echo "Removing user scripts"
    sudo rm -fv /mnt/home/${username}/*.sh

    if [[ -d ${HOME}/repos ]]; then
        echo "Copying repos"
        rsync -avP ${HOME}/repos /mnt/home/${username}/
    fi

    echo "Umounting"
    sudo umount -rl /mnt 2>&1 >/dev/null
}

