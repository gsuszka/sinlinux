#!/bin/env bash

export MAKEFLAGS="-j$(nproc)"

function add-or-modify-option() {
    local where=${1}
    local what=${2}
    local toWhat=${3}
    local afterWhat=${4}

    if grep -e ${what} ${where}; then
        sed -i "s/.*${what}.*/${toWhat}/g" ${where}
    else
        if [[ -z ${afterWhat} ]]; then
            echo ${toWhat} >>${where}
        else
            sed -i "/^${afterWhat}/a ${toWhat}" ${where}
        fi
    fi

}

echo "Enabling pacman parallel downloads"
add-or-modify-option /etc/pacman.conf "ParallelDownloads" "ParallelDownloads = $(nproc)" "\[options\]"
echo "Enabling pacman's... pacman :3"
add-or-modify-option /etc/pacman.conf "ILoveCandy" "ILoveCandy" "\[options\]"
echo "Enabling os-prober"
add-or-modify-option /etc/default/grub "GRUB_DISABLE_OS_PROBER" "GRUB_DISABLE_OS_PROBER=false"

echo "Setting hostname"
echo "sinight-archlinux" >>/etc/hostname

echo "Setting timezone"
ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
hwclock --systohc

echo "Setting locale"
sed -i 's/#pl_PL.UTF-8/pl_PL.UTF-8/g' /etc/locale.gen
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen

echo "Generating locale"
locale-gen

echo "Installing grub"
grub-install \
    --target=x86_64-efi \
    --bootloader-id=GRUB \
    --efi-directory=/boot/efi

echo "Generating grub config"
grub-mkconfig -o /boot/grub/grub.cfg

echo "Installing packages"
toInstall=(
    wget
    git
    vim
    docker
    python
    wpa_supplicant
    dialog
    openssh
    tree
    htop
    go
    zsh
    systemd-swap
    man
)
pacman -Sy --needed --noconfirm ${toInstall[@]}

echo "Root password"
passwd
useradd -m \
    -G mail,games,network,power,git,docker,users,video,storage,audio,wheel,realtime \
    -s /bin/zsh \
    sinight

echo "User password"
passwd sinight

EDITOR=vim visudo
