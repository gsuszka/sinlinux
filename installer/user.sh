#!/bin/env bash
set -e
echo "Current user $(whoami)"

export HOME=/home/$(whoami)
export MAKEFLAGS="-j$(nproc)"

if ! command yay; then
	echo "Installing yay"
	if [[ ! -d ${HOME}/yay ]]; then
		git clone http://aur.archlinux.org/yay ${HOME}/yay
	fi
	cd ${HOME}/yay
	makepkg -si
	cd -
	rm -rf ${HOME}/yay
fi

audioPkgs=(
	pipewire
	pipewire-jack
	pipewire-jack-dropin
	pipewire-pulse
	pipewire-media-session
	pavucontrol
	carla
	cadence
)

osPkgs=(
	nvidia
	nvidia-utils
	update-grub
	snapd
	timeshift
	shfmt
)

netPkgs=(
	networkmanager
	openssh
	openvpn
)

shellPkgs=(
	zsh-autosuggestions
	zsh-completions
	zsh-syntax-highlighting
	oh-my-zsh-git
	dtrx
	p7zip
	pigz
	tmux
	unzip
	unrar
	rsync
)

wmPkgs=(
	xorg
	xorg-xinit
	noto-fonts
	noto-fonts-emoji
	arandr
	lxappearance
)

apps=(
	telegram-desktop
	terminator
	dolphin
	konsole
	google-chrome-dev
	code
	ardour
	discord-canary
	spotify
)

plasmaPkgs=(
	plasma
	packagekit-qt5
	spectacle
)

yay -Syu --noconfirm --needed \
	${audioPkgs[@]} \
	${osPkgs[@]} \
	${netPkgs[@]} \
	${shellPkgs[@]} \
	${apps[@]}

echo "Enabling NetworkManager"
sudo systemctl enable --now NetworkManager
echo "Enabling PipeWire"
systemctl --user enable --now pipewire

if [[ ! -d ${HOME}/.oh-my-zsh ]]; then
	echo "Installing oh-my-zsh"
	/usr/share/oh-my-zsh/tools/install.sh &
fi

echo "Removing yay cache"
yay -Sc --noconfirm
yay -Scc --noconfirm

echo "Installing pip"
curl -sSL https://bootstrap.pypa.io/get-pip.py | python3 -
echo "Installing poetry"
curl -sSL https://install.python-poetry.org | python3 -

echo PATH=${PATH}:${HOME}/.local/bin >>${HOME}/.zshrc

echo "Upgrading pip"
pip install -U pip
echo "Upgrading poetry"
poetry self update --preview

sudo vim /usr/share/pipewire/pipewire.conf

# omz plugin enable docker python zsh-syntax-highlighting zsh-autosuggestions
