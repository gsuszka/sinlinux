#!/bin/bash -e

export RVG_PATH=${PATH}
export SIN_SINLINUX_ROOT=$(dirname ${0})
export SIN_SCRIPTS_DIR=${SIN_SINLINUX_ROOT}/scripts
export SIN_REPOS_ROOT=${HOME}/repos
export SIN_VPN_CERT_ROOT=${SIN_REPOS_ROOT}

local _sin_scripts=(
    vpn
    helpers
    upgrade
    python
    scala
    cuda
    aliases
    pkgs
    repos
    boot
    completions
)

for script in ${_sin_scripts}; do
    source ${SIN_SCRIPTS_DIR}/${script}.sh
done

source ${SIN_SINLINUX_ROOT}/installer/install.sh

if sin-check-and-install-exec neofetch && sin-check-and-install-exec jp2a; then
    width=35
    color_depth=24
    img=$(jp2a ${SIN_SINLINUX_ROOT}/res/logo2.png \
        --colors \
        --width=${width} \
        --color-depth=${color_depth})
    neofetch \
        --gap -$((${width} * 23)) \
        --source ${img}
fi

echo "Welcome to SinLinux UwU"

if [[ -d ${HOME}/.local/bin ]]; then
    export PATH=${PATH}:${HOME}/.local/bin
fi

if [[ -d ${HOME}/.cargo/bin ]]; then
    export PATH=${PATH}:${HOME}/.cargo/bin
fi

export PATH=${PATH}:$(dirname $(which python))
export MAKEFLAGS="-j$(nproc)"

eval "$(pyenv init --path)"
eval "$(pyenv init -)"
eval $(ssh-agent -s)

# To activate virtualenv
cd .
